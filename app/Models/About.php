<?php

namespace App\Models;

use App\Observers\AboutObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class About extends Model
{
    use HasFactory, Notifiable;
    protected static function boot() {
        parent::boot();
        static::observe(AboutObserver::class);
    }
    protected $table = "abouts";
    protected $fillable = [
        'about_image',
        'description',
        'skills',
    ];
}

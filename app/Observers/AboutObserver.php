<?php

namespace App\Observers;

use App\Models\About;
use Illuminate\Support\Facades\File;

class AboutObserver
{
    public function creating(About $about)
    {
        if(request()->about_image)
        {
            $file_name = time().'.'.request()->about_image->extension();
            $about->about_image = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(About $about)
    {
        if(request()->about_image)
        {
            $path = public_path('about-uploads/about');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->about_image->move($path, $about->about_image);
        }
    }

    public function updating(About $about)
    {
        if(request()->about_image) {

            // Old file delete code
            $path = public_path('user-uploads/users/');
            $this->deleteFile($path.$about->about_image);

            $file_name = time().'.'.request()->file->extension();
            $about->about_image = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(About $about)
    {
        if(request()->about_image) {

            $path = public_path('about-uploads/about/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->about_image->move($path, $about->about_image);
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(About $about)
    {
        $path = public_path('about-uploads/about/');

        // Old file delete code
        $this->deleteFile($path.$about->about_image);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(About $about)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(About $about)
    {
        //
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }

}

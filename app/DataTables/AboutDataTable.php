<?php

namespace App\DataTables;

use App\Models\About;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AboutDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('abouts.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i></a>

                    <a class="btn btn-warning" href="' . route('abouts.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>

                    <a class="btn btn-danger" ' . route('abouts.destroy' , [$row->id]) . '">
                        <i class="fa fa-trash"></i>
                    </a>';

                return $action;
            });

            // ->addColumn('description', function ($row) {
            //     $about = "";
            //         foreach ($row->about as $abouts) {
            //             $about = $abouts->description;
            //         }
            //         return  $about;

            // });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\About $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(About $model)
    {
        $request = $this->request();
        $model = $model->select('id', 'about_image', 'description', 'skills');
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->setTableId('abouts-table')
        ->columns([
                'id',
                'about_image',
                'description',
                'skills',
                'action'
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'about_image',
            'description',
            'skills'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'About_' . date('YmdHis');
    }
}

<?php

namespace App\DataTables;

use App\Models\projectDataTable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class projectDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'projectdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\projectDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(projectDataTable $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
     public function html()
    {
        return $this->builder()
        ->columns([
                'id',
                'title',
                'date',
                'project_image',
                'description',
                'skills'
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
     protected function getColumns()
    {
        return [
            'id',
            'about_image',
            'description',
            'skills'
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'project_' . date('YmdHis');
    }
}

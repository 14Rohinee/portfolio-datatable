@extends('layouts.dashboard')

@section('title')
    Add User
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection


@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create User Form</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Users</a>
                </li>
                <li class="active">
                    <strong>Add User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <button type="submit" class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save User</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('abouts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>About Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter about yourself" name="description" id="description" value="{{ old('description') }}">
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('skills') has-error @enderror">
                                        <label for="skills">Skills <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Your skill" name="skills" id="skills" value="{{ old('skills') }}">
                                        @error('skills')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Uploaded About Image</h5>
                            </div>
                            <div class="ibox-content">
                                <input type="file" class="dropify" name="about_image" />
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <script>
        $('.dropify').dropify();

        $(document).on('click', '#save-data', function(){
            $('form').submit();
        });


    </script>

@endsection






<link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>

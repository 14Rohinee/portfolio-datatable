@extends('layouts.dashboard')

@section('css')
   <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li class="active">
                <strong>About</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('abouts.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add About
            </a>
        </div>
    </div>
@endsection

@section('content')
    {!! $dataTable->table() !!}
@endsection

@section('script')
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>

    {!! $dataTable->scripts() !!}
@endsection
